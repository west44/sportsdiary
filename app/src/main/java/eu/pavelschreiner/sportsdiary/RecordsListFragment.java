package eu.pavelschreiner.sportsdiary;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import eu.pavelschreiner.sportsdiary.tools.RecordListAdapter;
import eu.pavelschreiner.sportsdiary.tools.RecordViewModel;


public class RecordsListFragment extends Fragment {
    private RecordListAdapter recordListAdapter;
    private RecordViewModel recordViewModel;
    private  static String TAG = RecordsListFragment.class.getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recordListAdapter = new RecordListAdapter(getContext());
        recordViewModel = new RecordViewModel(getContext());
        recordViewModel.setRecordListAdapter(recordListAdapter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_records_list, container, false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.records_list);
        recyclerView.setAdapter(recordListAdapter);
        recyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        loadSportRecordsList();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.nav_records_list));
    }

    /**
     * Load remote (firebase) and local (sqlite) sport records
     */
    private void loadSportRecordsList() {
        recordViewModel.getRemoteRecords();
        // init loader manager for load local data from SQlite DB
        getLoaderManager().initLoader(0, null, recordViewModel);
    }
}
