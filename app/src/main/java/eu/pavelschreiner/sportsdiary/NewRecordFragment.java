package eu.pavelschreiner.sportsdiary;

import android.content.ContentValues;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import eu.pavelschreiner.sportsdiary.objects.SportRecord;
import eu.pavelschreiner.sportsdiary.tools.RecordViewModel;

/**
 * Created by west on 29.12.2017.
 */

public class NewRecordFragment extends Fragment {
    private View view;
    private RecordViewModel recordViewModel;
    private String storageType;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create_record_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_record, container, false);
        setHasOptionsMenu(true);
        storageType = getString(R.string.storage_local); // set default value of radio button in UI
        recordViewModel = new RecordViewModel(getContext());
        RadioGroup storageRadioGroup = (RadioGroup) view.findViewById(R.id.storage_radiogroup);
        storageRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.storage_local) storageType = getString(R.string.storage_local);
                else storageType = getString(R.string.storage_remote);
            }
        });
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            recordViewModel.saveRecord(storageType, getSportRecord());
        }
        return true;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.nav_new_record));
    }

    /**
     * Load data from UI into SportRecord instance
     * @return SportRecord
     */
    private SportRecord getSportRecord() {
        SportRecord sportRecord = new SportRecord();
        EditText recordTitle = (EditText) view.findViewById(R.id.record_title);
        EditText recordPlace = (EditText) view.findViewById(R.id.record_place);
        EditText recordDuration = (EditText) view.findViewById(R.id.record_duration);
        sportRecord.setTitle(String.valueOf(recordTitle.getText()));
        sportRecord.setPlace(String.valueOf(recordPlace.getText()));
        sportRecord.setDuration(String.valueOf(recordDuration.getText()));
        return sportRecord;
    }
}
