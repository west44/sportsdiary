package eu.pavelschreiner.sportsdiary.tools;

import android.content.Context;
import android.database.Cursor;

import eu.pavelschreiner.sportsdiary.database.DbHelper;

/**
 * Created by west on 1.1.2018.
 */

public class SportRecordLoader extends SimpleCursorLoader {
    private DbHelper dbHelper;

    public SportRecordLoader(Context context) {
        super(context);
        dbHelper = new DbHelper(context);
    }

    @Override
    public Cursor loadInBackground() {
        return dbHelper.getAllSportRecords();
    }
}
