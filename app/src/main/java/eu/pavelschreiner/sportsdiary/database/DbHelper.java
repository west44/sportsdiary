package eu.pavelschreiner.sportsdiary.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.widget.Toast;

import eu.pavelschreiner.sportsdiary.R;
import eu.pavelschreiner.sportsdiary.objects.SportRecord;

public class DbHelper extends SQLiteOpenHelper {
    private static String TAG = DbHelper.class.getSimpleName();
    private static final String DATABASE_NAME = "sportrecords";
    private static final int DATABASE_VERSION = 1;
    private SQLiteDatabase db;
    private Context context;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        db = this.getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        SportRecordTable.onCreate(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        SportRecordTable.onUpgrade(database, oldVersion, newVersion);
    }

    /**
     * async save of sport record instance
     * @param sportRecord
     */
    public void save(final SportRecord sportRecord) {
        new AsyncTask<SportRecord, Void, Long>() {
            @Override
            protected Long doInBackground(SportRecord... sportRecords) {
                return db.insert(SportRecordTable.TABLE, null, getRecordContentValues(sportRecord));
            }

            @Override
            protected void onPostExecute(Long result) {
                super.onPostExecute(result);
                Toast.makeText(context, (result > 0) ? R.string.save_record_ok : R.string.save_record_fail, Toast.LENGTH_LONG).show();
            }
        }.execute(sportRecord);
    }

    /**
     * get all sport records from local db
     * @return Cursor
     */
    public Cursor getAllSportRecords() {
        return db.rawQuery("SELECT * FROM " + SportRecordTable.TABLE, null);
    }

    /**
     * create instance of ContentValues from SportRecord instance
     * @param record
     * @return ContentValues
     */
    private ContentValues getRecordContentValues(SportRecord record) {
        ContentValues values = new ContentValues();
        values.put("title", record.getTitle());
        values.put("place", record.getPlace());
        values.put("duration", record.getDuration());
        return values;
    }
}
