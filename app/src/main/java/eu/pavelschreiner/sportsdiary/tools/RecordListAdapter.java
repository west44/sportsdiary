package eu.pavelschreiner.sportsdiary.tools;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import eu.pavelschreiner.sportsdiary.R;
import eu.pavelschreiner.sportsdiary.objects.SportRecord;

public class RecordListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static String TAG = RecordListAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<SportRecord> records;
    private static final int EMPTY_VIEW = 10;

    public RecordListAdapter(Context context) {
        this.context = context;
        records = new ArrayList<>();
    }

    /**
     * overloaded method for add new sport records in records ArrayList and redraw RecyclerView in UI
     */
    public void addSportRecords(Cursor cursor) {
        addSportRecords(getSportRecordsArrayList(cursor));
    }
    public void addSportRecords(ArrayList<SportRecord> sportRecords) {
        for (SportRecord sportRecord : sportRecords) records.add(sportRecord);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == EMPTY_VIEW) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.empty_row, parent, false);
            EmptyViewHolder evh = new EmptyViewHolder(view);
            return evh;
        }
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sport_record_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) { // use only if there are some sport records to show
            ViewHolder viewHolder = (ViewHolder) holder;
            SportRecord SportRecord = records.get(position);
            if (SportRecord.getTitle() != null)  viewHolder.itemTitle.setText(SportRecord.getTitle());
            if (SportRecord.getPlace() != null)  viewHolder.itemPlace.setText(SportRecord.getPlace());
            if (SportRecord.getDuration() != null)  viewHolder.itemDuration.setText(SportRecord.getDuration());
            if (SportRecord.getStorage().equals(context.getString(R.string.storage_local)))
                viewHolder.itemView.setBackgroundResource(R.color.colorLocalStorage);
            else if (SportRecord.getStorage().equals(context.getString(R.string.storage_remote)))
                viewHolder.itemView.setBackgroundResource(R.color.colorRemoteStorage);
            viewHolder.itemView.setTag(records.get(position));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (records.isEmpty()) return EMPTY_VIEW; // no records at this time
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        if (records.size() == 0) return 1; // 1 because it will show one row with empty text message
        else return records.size();
    }

    private ArrayList<SportRecord> getSportRecordsArrayList(Cursor c) {
        ArrayList<SportRecord> sportRecords = new ArrayList<>();
        if (c.getCount() > 0 && c.moveToFirst()) {
            for (int i = 0; i < c.getCount(); i++) {
                Log.d(TAG, "record: " + c.getString(c.getColumnIndex("title")) + ", place: " + c.getString(c.getColumnIndex("place")));
                SportRecord sportRecord = new SportRecord();
                sportRecord.setTitle(c.getString(c.getColumnIndex("title")));
                sportRecord.setPlace(c.getString(c.getColumnIndex("place")));
                sportRecord.setDuration(c.getString(c.getColumnIndex("duration")));
                sportRecord.setStorage(context.getString(R.string.storage_local));
                sportRecords.add(sportRecord);
                c.moveToNext();
            }
        }
        c.close();
        return sportRecords;
    }

    // use for regular sport records
    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView itemTitle, itemPlace, itemDuration;

        ViewHolder(View view) {
            super(view);
            itemTitle = (TextView) view.findViewById(R.id.record_title);
            itemPlace = (TextView) view.findViewById(R.id.record_place);
            itemDuration = (TextView) view.findViewById(R.id.record_duration);
        }
    }

    // use for empty message when there is no sport records
    class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }
}
