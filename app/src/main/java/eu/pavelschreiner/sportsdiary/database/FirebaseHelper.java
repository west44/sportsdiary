package eu.pavelschreiner.sportsdiary.database;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import eu.pavelschreiner.sportsdiary.R;
import eu.pavelschreiner.sportsdiary.objects.SportRecord;
import eu.pavelschreiner.sportsdiary.tools.RecordViewModel;

public class FirebaseHelper {
    private static String TAG = FirebaseHelper.class.getSimpleName();
    private DatabaseReference firebaseDatabase;
    private RecordViewModel recordViewModel;
    private Context context;

    public FirebaseHelper(Context context, RecordViewModel recordViewModel) {
        this.context = context;
        this.recordViewModel = recordViewModel;
        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
    }

    /**
     * save sport record in FireBase
     * @param sportRecord
     */
    public void save(SportRecord sportRecord) {
        firebaseDatabase.child("sport_records").push().setValue(sportRecord).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(context, task.isSuccessful() ? R.string.save_record_ok : R.string.save_record_fail, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * get sport records form Firebase and send data back in RecordViewModel
     */
    public void getSportRecords() {
        firebaseDatabase.getRoot().child("sport_records").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<SportRecord> sportRecords = new ArrayList<SportRecord>();
                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    sportRecords.add(child.getValue(SportRecord.class));
                }
                recordViewModel.getRemoteRecordsCallback(sportRecords);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

}
