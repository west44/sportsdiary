package eu.pavelschreiner.sportsdiary.database;

import android.database.sqlite.SQLiteDatabase;

public class SportRecordTable {
    public static String
            TABLE = "sport_record";
    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLE
            + " (srid INTEGER PRIMARY KEY AUTOINCREMENT, "

            + "title TEXT, "
            + "place TEXT, "
            + "duration INT, "
            + "stamp DATETIME DEFAULT CURRENT_TIMESTAMP "
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }
}
