package eu.pavelschreiner.sportsdiary.tools;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import java.util.ArrayList;

import eu.pavelschreiner.sportsdiary.R;
import eu.pavelschreiner.sportsdiary.database.DbHelper;
import eu.pavelschreiner.sportsdiary.database.FirebaseHelper;
import eu.pavelschreiner.sportsdiary.objects.SportRecord;

public class RecordViewModel implements LoaderManager.LoaderCallbacks<Cursor>  {
    private static String TAG = RecordViewModel.class.getSimpleName();
    private Context context;
    private RecordListAdapter recordListAdapter;
    private DbHelper dbHelper;
    private FirebaseHelper firebaseHelper;

    public RecordViewModel(Context context) {
        this.context = context;
        dbHelper = new DbHelper(context);
        firebaseHelper = new FirebaseHelper(context, this);
    }

    public void setRecordListAdapter(RecordListAdapter recordListAdapter) {
        this.recordListAdapter = recordListAdapter;
    }

    /**
     * save sport record depending on the storage param
     * @param storage
     * @param sportRecord
     */
    public void saveRecord(String storage, SportRecord sportRecord) {
        if (storage.equals(context.getString(R.string.storage_local))) dbHelper.save(sportRecord);
        else if (storage.equals(context.getString(R.string.storage_remote))) firebaseHelper.save(sportRecord);
    }

    public void getRemoteRecords() {
        firebaseHelper.getSportRecords();
    }

    public void getRemoteRecordsCallback(ArrayList<SportRecord> sportRecords) {
        for (SportRecord sportRecord: sportRecords) sportRecord.setStorage(context.getString(R.string.storage_remote));
        addRecordsInAdapter(sportRecords);
    }

    private void addRecordsInAdapter(ArrayList<SportRecord> sportRecords) {
        if (recordListAdapter != null) {
            recordListAdapter.addSportRecords(sportRecords);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new SportRecordLoader(context);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        recordListAdapter.addSportRecords(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {}
}
